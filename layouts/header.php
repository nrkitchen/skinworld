
<nav class="navbar">
  <div class="container-fluid">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#main-nav" aria-expanded="false">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
    </div>
   
    <div class="collapse navbar-collapse" id="main-nav">
      <ul class="nav navbar-nav navbar-right">
        <div class="header-nav"><?php include('nav.php'); ?>
          
           <?php  if(isUser()) { ?>
          <li><a href="<?php echo BASE_URL; ?>affiliates" title="Free Credits"><i class="fa fa-star" aria-hidden="true"></i></a></li>
          <li><a class="btn btn-primary green" data-toggle="modal" data-target="#addFunds"><i class="fa fa-plus" aria-hidden="true"></i> Add Funds</a></li>
          <li><a class="btn btn-primary green" href="<?php echo BASE_URL; ?>/affiliates"><i class="fa fa-star" aria-hidden="true"></i> FREE COINS</a></li>
        <?php } ?>
        </div>
      
          
      </ul>
    </div>
  </div>
</nav>