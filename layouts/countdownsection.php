<style>
/****************timer info css start********************/

       #timersection .header{display: flex;height: 70px;padding: 0 32px 0 50px;background: rgba(18,23,34,.7);box-shadow: 0 5px 25px rgba(0,0,0,.15);font-size: 14px;z-index: 3;}

      @media (min-width: 1024px){
      #timersection .header {font-size: 15px;}
      }

      #timersection  .header {width: 94%;background: #131d29;border-bottom: 1px solid hsla(0,0%,100%,.08);}

      @media (min-width: 980px){
      #timersection  .header {background: rgba(0,0,0,.2);}
      }

      #timersection  .header__ny-sales {width: 100%;}
      #timersection  .header__ny-sales {display: flex;flex-direction: column;margin-left: -50px;}
      #timersection  .ny-sales {display: flex;flex: 1 1;padding-left: 36px;background-image: url(img/new/icon/sales-bg.webp);background-size: auto 100%;background-repeat: no-repeat;position: relative;color: white;}
      #timersection  .ny-sales__counter,#timersection  .ny-sales__title {display: flex;align-items: center;}
      #timersection  .ny-sales__title {padding-left: 65px;text-transform: uppercase;margin-right: 25px;font-size: 15px;
          line-height: 18px;width: 180px;font-weight: 700;}

      @media (min-width: 1324px){
        #timersection  .ny-sales__title {width: auto;margin-right: 25px;line-height: 21px;font-size: 19px;}
      }

      #timersection  .ny-sales__title:before {position: absolute;content: "";display: block;width: 44px;height: 42px;left: 45px;top: 50%;transform: translateY(-50%);background: url(img/new/icon/sales-icon.svg?2);}
      #timersection .ny-sales__counter, .ny-sales__title {display: flex;align-items: center;}
      #timersection .sales-counter {display: flex;flex-direction: column;}
      #timersection .sales-counter__text {display: flex;font-size: 12px;margin-bottom: 2px;padding-left: 4px;color: #b1bbca;
          margin-right: 20px;}
      #timersection .sales-counter__counter, .sales-counter__numbers-group {display: flex;align-items: center;}
      #timersection .sales-counter__numbers-group {margin: 0 4px;position: relative;}
      #timersection .sales-counter__counter, .sales-counter__numbers-group {display: flex;align-items: center;}
      #timersection .sales-counter__number{border-radius: 5px ;}
      #timersection .sales-counter__number {font-size: 12px;line-height: 14px;border: 1px solid #4c525b;display: flex;
         height: 40px;
    width: 38px;
    padding: 10px 20px;align-items: center;justify-content: center;}

      @media (min-width: 1120px){
      #timersection .sales-counter__number {      font-size: 18px;
    line-height: 18px;
    height: 40px;
    width: 38px;
    padding: 10px 20px;}
      }
      #timersection .sales-counter__numbers-group:after {content: ":";margin-left: 8px;font-size: 18px;}
      #timersection .sales-counter__numbers-group:last-child:after{display:none}
      #timersection .header__nav,#timersection .header__nav-projects,#timersection .header__settings,#timersection .header__settings-socials,#timersection .header__socials {height: 100%;}
      #timersection .header__nav-projects,#timersection .header__settings-socials {display: flex;}
      #timersection .header__nav-projects {flex-grow: 1;flex-direction: row-reverse;margin: 0 25px;align-items: center;}
      #timersection .header__nav-projects,#timersection .header__settings-socials {display: flex;}
      #timersection .header-settings {padding: 0;margin: 0 -12px;display: flex;height: 100%;}
      #timersection .with-dropdown {position: relative;}
      #timersection .header-settings__item {display: block;margin: 0 12px;height: 100%;}
      #timersection .header-settings__link {position: relative;display: flex;height: 100%;align-items: center;
          color: #fff;transition: .2s ease;}
      #timersection .header-settings__link_lang {font-size: 0;}

      @media (min-width: 1270px){
      #timersection .header-settings__link_lang {font-size: 15px;transform: none;}
      }

      #timersection .header-settings__link_lang .icon {transform: scale(1.3);}
      #timersection .header-settings__link .icon {margin-right: 8px;}
      #timersection .with-dropdown__dropdown {position: absolute;display: inline-block;opacity: 0;visibility: hidden;   transition: .2s ease;left: -21px;margin-top: -12px;}
      #timersection .with-dropdown:hover .with-dropdown__dropdown {visibility: visible;opacity: 1;}
      #timersection .dropdown {border-radius: 5px;border: 1px solid hsla(0,0%,100%,.08);padding: 0 20px;background: rgba(18,23,34,.8);min-width: 164px;}
      #timersection .dropdown__link {display: flex;align-items: center;color: #b1bbca;margin: 20px 0;transition: .1s ease-in-out;}
      #timersection .dropdown__link .icon {margin-right: 8px;margin-top: -2.5px;}
      #timersection .header__socials {margin-left: 25px;}

      @media (min-width: 1024px)
      {
        #timersection .header__socials { margin-left: 34px;}
      }
      #timersection .header__socials {display: flex;align-items: center;}
      #timersection .socials-links {display: flex;align-items: center;}
      #timersection .socials-links__link {position: relative;display: block;margin-left: 15px;width: 24px;height: 24px;}
      #timersection .socials-links__link_facebook:before {background-image: url(img/new/icon/icon_facebook.svg?3);}
      #timersection .socials-links__link:before {z-index: 2;opacity: 0;visibility: hidden;}
      #timersection .socials-links__link:after,#timersection .socials-links__link:before {content: "";position: absolute;
          width: 100%;height: 100%;top: 50%;left: 50%;transform: translate(-50%,-50%);transition: .2s ease-in-out;
          background-size: 100%;background-position: 50%;background-repeat: no-repeat;}
      #timersection .socials-links__link_facebook:after {background-image: url(img/new/icon/icon_facebook_nocolor.svg?3);}
      #timersection .socials-links__link:after {z-index: 1;}
      #timersection .socials-links__link_instagram:before {background-image: url(img/new/icon/icon_instagram.svg?3);}
      #timersection .socials-links__link_instagram:after {background-image: url(img/new/icon/icon_instagram_nocolor.svg?3);}
      #timersection .socials-links__link_youtube:before {background-image: url(img/new/icon/icon_youtube.svg?3);}
      #timersection .socials-links__link_youtube:after {background-image: url(img/new/icon/icon_youtube_nocolor.svg?3);}
      #timersection .socials-links__link_steam:before {background-image: url(img/new/icon/icon_steam.svg?3);}
      #timersection .socials-links__link_steam:after {background-image: url(img/new/icon/icon_steam_nocolor.svg?3);}
      #timersection .socials-links__link_twitter:before {background-image: url(img/new/icon/icon_twitter.svg?4);}
      #timersection .socials-links__link_twitter:after {background-image: url(img/new/icon/icon_twitter_nocolor.svg?4);}
      #timersection .main-layout__content {display: flex;flex-direction: column;min-width: 980px;width: 100%;flex-grow: 1;}
      #timersection.content {min-width: 780px;display: flex;flex-direction: column;flex-grow: 1;}
      #timersection .site-statistics {background: transparent;border-top: 1px solid hsla(0,0%,100%,.08);position: relative;
          padding: 5px 0 6px 50px;display: flex;flex-wrap: wrap;align-items: center;font-size: .9em;order: 1;}

      @media (min-width: 1260px){
      #timersection .site-statistics {border-top: none;border-bottom: 1px solid hsla(0,0%,100%,.08);order: 0;flex-wrap: nowrap;}
      }
      #timersection .site-statistics__item {margin-right: 30px;}

      @media (min-width: 1260px){
      #timersection .site-statistics__item {width: auto;}
      }
      #timersection .site-statistics__item:first-child {min-width: 145px;}

      @media (min-width: 1260px){
      #timersection .site-statistics__item:first-child {min-width: 200px;}
      }
      #timersection .site-stat-counter {width: 100%;display: inline-flex;position: relative;justify-content: center;
          flex-direction: column;height: 50px;}

      @media (min-width: 1260px){
      #timersection .site-stat-counter__num, .site-stat-counter__text {margin-left: 50px;}
      }

      #timersection .site-stat-counter__num {line-height: 1.2em;color: #fff;font-size: 1.7em;}
      #timersection .site-stat-counter__text {font-size: 1em;line-height: 1em;padding: 4px 0 0;color: #b1bbca;transition: color .25s ease-in-out;}
      #timersection .site-stat-counter_cases .site-stat-counter__num,#timersection .site-stat-counter_cases .site-stat-counter__text {margin-left: 55px;}

      @media (min-width: 1260px){
      #timersection .site-stat-counter:before {content: "";position: absolute;background-repeat: no-repeat;background-size: auto 100%;}
      }
      #timersection .site-stat-counter_contracts:before {width: 41px;height: 41px;background-image: url(img/new/icon/icon_contract.svg);}
      #timersection .site-stat-counter_cases:before {width: 45px;height: 45px;background-image: url(img/new/icon/icon_case.svg);}
      #timersection .site-stat-counter_battles:before {width: 43px;height: 38px;background-size: 100%;background-image: url(img/new/icon/icon_battle.svg);}
      #timersection .site-stat-counter_users:before {width: 43px;height: 43px;background-image: url(img/new/icon/icon_user.svg);}
      #timersection .site-stat-counter_online:before {width: 38px;height: 38px;background-image: url(img/new/icon/icon_locator.svg);
          background-position: 50%;animation: spin 8s linear infinite;backface-visibility: hidden;}
      @keyframes spin{0%{transform:rotate(0)}to{transform:rotate(1turn)}}
      #timersection .site-statistics__item a{text-decoration:none;}

/****************timer info css end********************/

</style>
<div class="main-layout__content">
  <div class="content" id="timersection">
   <header class="header">
    <div class="header__ny-sales">
     <div class="ny-sales row1">
      <div class="col-lg-5 col-md-4 col-sm-12 col-xs-6 ny-sales__title">
        <span>Katowice Major 2019 25%</span>
      </div>
      <div class="col-lg-3 col-md-4 col-sm-12 col-xs-6 ny-sales__counter">
        <div class="sales-counter">
         <div class="sales-counter__text">Time left</div>
       <div class="sales-counter__counter"  id="countdowntimer"></div>         
        </div>
      </div>
      <div class="col-lg-4 col-md-4 col-sm-12 col-xs-6">
         <div class="socials-links">
      <ul>
        <li>
        <a target="_blank" href="https://www.facebook.com/csgonetcase" class="socials-links__link socials-links__link_facebook"></a>
      </li><li>
        <a target="_blank" href="https://www.instagram.com/csgo.net_official/" class="socials-links__link socials-links__link_instagram"></a>
        </li><li>
        <a target="_blank" href="https://www.youtube.com/channel/UCaiTUij0Y6zHcPIKGJ36ZNg" class="socials-links__link socials-links__link_youtube"></a>
        </li><li>
        <a target="_blank" href="https://steamcommunity.com/groups/CSGO-net" class="socials-links__link socials-links__link_steam"></a>
        </li><li>
        <a target="_blank" href="https://twitter.com/CSGOnet_case" class="socials-links__link socials-links__link_twitter"></a>
        </li><li>
          <a id="toggleMute">
        <?php
          $mute = isset($_COOKIE['mute']) && $_COOKIE['mute'] == 1 ? 'off' : 'up';
          echo '<i class="fa fa-volume-'.$mute.'" aria-hidden="true"></i>';
        ?>
        </a>
        </li><li>

          <div class="right">
        <h5>Currently Online:</h5><h6><span id="onlineUsers"><?= isset($_COOKIE['online']) ? $_COOKIE['online'] : null ?></span> Players</h6>
      </div>
      </li>
      </ul>
      </div>
        

      </div>
    </div>
  </div>
 
  </header>
  <div class="cases-main-inner">
   <div class="site-statistics">
    <div class="site-statistics__item">
        <?php $getRecord=getDatasingle("SELECT count(*) AS totalCase FROM `case`"); ?>
      <a href="/" class="site-stat-counter site-stat-counter_cases">
        <div class="site-stat-counter__num"><?php echo $getRecord['totalCase'];?></div>
        <div class="site-stat-counter__text">Cases</div>
      </a>
    </div>

    <div class="site-statistics__item">
      <a href="/" class="site-stat-counter site-stat-counter_battles">
        <div class="site-stat-counter__num">
        <?php $getRecordItem=getDatasingle("SELECT count(*) AS totalCase FROM `cases_items`"); ?>
        <?php echo $getRecordItem['totalCase'];?></div>
        <div class="site-stat-counter__text">Battles</div>
      </a>
    </div>
    <div class="site-statistics__item">
      <a href="/" class="site-stat-counter site-stat-counter_users">
        <div class="site-stat-counter__num">1 241 </div>
        <div class="site-stat-counter__text">Users</div>
      </a>
    </div>
    <div class="site-statistics__item">
      <a href="/" class="site-stat-counter site-stat-counter_online">
        <div class="site-stat-counter__num" id="OnlineUser">640</div>
        <div class="site-stat-counter__text">Online</div>
      </a>
    </div>

  <div class="site-statistics__item">
       <?php if(isUser()) { ?>
       <ul>
          <li><a href="<?php echo BASE_URL; ?>affiliates" title="Free Credits"><i class="fa fa-star" aria-hidden="true"></i></a></li>
          <li><a class="btn btn-primary green" data-toggle="modal" data-target="#addFunds"><i class="fa fa-plus" aria-hidden="true"></i> Add Funds</a></li>
          <li><a class="btn btn-primary green" href="<?php echo BASE_URL; ?>/affiliates"><i class="fa fa-star" aria-hidden="true"></i> FREE COINS</a></li>
        </ul>
        <?php } ?>
      </div>
   </div>
  </div>
 </div>
</div>

<script type="text/javascript">
  
  var countDownDate = new Date("feb 25, 2019 15:37:25").getTime();

// Update the count down every 1 second
var x = setInterval(function() {

  // Get todays date and time
  var now = new Date().getTime();
    
  // Find the distance between now and the count down date
  var distance = countDownDate - now;
    
  // Time calculations for days, hours, minutes and seconds
  var days = Math.floor(distance / (1000 * 60 * 60 * 24));
  var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
  var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
  var seconds = Math.floor((distance % (1000 * 60)) / 1000);
    
  // Output the result in an element with id="demo"
  document.getElementById("countdowntimer").innerHTML = "  <div class='sales-counter__numbers-group'>               <div class='sales-counter__number'>"+days+"</div>             </div>             <div class='sales-counter__numbers-group'>               <div class='sales-counter__number'>" + hours + "</div>             </div>             <div class='sales-counter__numbers-group'>               <div class='sales-counter__number'>"+ minutes + "</div>             </div>             <div class='sales-counter__numbers-group'>               <div class='sales-counter__number'>" + seconds + "</div>  ";
    
  // If the count down is over, write some text 
  if (distance < 0) {
    clearInterval(x);
    document.getElementById("countdowntimer").innerHTML = "EXPIRED";
  }

}, 1000);

/// for online user
var xy = setInterval(function() {
document.getElementById("OnlineUser").innerHTML =randomIntFromInterval(650,700);
  }, 8000);

function randomIntFromInterval(min,max) // min and max included
{
    return Math.floor(Math.random()*(max-min+1)+min);
}

</script>
