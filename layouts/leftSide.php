 <div class="site_logo">
  <a class="navbar-brand logo" href="<?php echo BASE_URL; ?>"><img src="<?php echo BASE_URL; ?>/img/csgodisease.png" class="img-responsive" /></a>
    </div>

<?php if(isUser()) { ?>
  <div class="profile">
  	<a href="<?php echo BASE_URL; ?>?logout" style="margin-right:15px;" title="Logout" class="logoutlnk"><i class="fa fa fa-times" aria-hidden="true"></i></a>
      <a href="<?php echo BASE_URL; ?>inventory" class="inventory">
    <img src="<?php if(!empty(getUser('avatar'))) { echo getUser('avatar'); } else{ echo 'img/new/default_img.png'; }  ?>" class="img-responsive" />
    <h5><?= getUser('name') ?></h5>
</a>
    <h6>Balance: $<span id="balance"><?= getBalance() ?></span></h6>
  </div>
<?php }else{ ?>
<div class="profile SignBox">
	<a class="loginlnk" href="<?php echo BASE_URL; ?>?login">
	     <div class="authblock_toptext">Sign in with</div>
			<div class="authblock_bottext">Steam</div>
	</a>
</div>
<?php 
}
?>
<ul class="left-nav">
  <?php include('nav.php'); ?>
</ul>
<?php include('footer.php'); ?>
